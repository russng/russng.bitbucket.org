var indexSectionsWithContent =
{
  0: "_abcdefghilmnoprstuvw",
  1: "hr",
  2: "bcefhimorstu",
  3: "r",
  4: "abcdefghilmnoprstuvw",
  5: "rs",
  6: "_cdhpr"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros"
};

