var searchData=
[
  ['saddr',['saddr',['../structruss__svr.html#a9247b77c1e3b0065da4ad4c1342baed7',1,'russ_svr']]],
  ['sconn',['sconn',['../structruss__sess.html#a38b853f2a831c5c1a5e92288d0b9389e',1,'russ_sess::sconn()'],['../structhelper__data.html#a38b853f2a831c5c1a5e92288d0b9389e',1,'helper_data::sconn()']]],
  ['sd',['sd',['../structruss__cconn.html#a06b0afe769d54ae94259d8532bc878b0',1,'russ_cconn::sd()'],['../structruss__sconn.html#a06b0afe769d54ae94259d8532bc878b0',1,'russ_sconn::sd()']]],
  ['sections',['sections',['../structruss__conf.html#a6397b95f348a972f557f0017f6ace211',1,'russ_conf']]],
  ['spath',['spath',['../structruss__req.html#aac73a1185fed83f2fe62cf06d2ed92ca',1,'russ_req::spath()'],['../structruss__sess.html#a55053e20f3dbf9ad437d65431385f5e9',1,'russ_sess::spath()']]],
  ['str',['str',['../structruss__optable.html#af25d6dc49269fa2003ac7c7fa6f13915',1,'russ_optable']]],
  ['streams',['streams',['../structruss__relay.html#a34a8d6943a4e7c6a95669bd7f35356f9',1,'russ_relay']]],
  ['svr',['svr',['../structruss__sess.html#acbcd4d590e6fa4139fb09308871623b7',1,'russ_sess::svr()'],['../structhelper__data.html#acbcd4d590e6fa4139fb09308871623b7',1,'helper_data::svr()']]],
  ['sysfds',['sysfds',['../structruss__cconn.html#a924296c186857e34d17dfb5002b157e7',1,'russ_cconn::sysfds()'],['../structruss__sconn.html#a924296c186857e34d17dfb5002b157e7',1,'russ_sconn::sysfds()']]]
];
