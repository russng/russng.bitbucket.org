var structruss__svcnode =
[
    [ "autoanswer", "structruss__svcnode.html#aa682db27c8180700165db1398964b3c8", null ],
    [ "children", "structruss__svcnode.html#ac9599965363069215a3014fefe3e5bf4", null ],
    [ "handler", "structruss__svcnode.html#af022afea2deca3e904e94f1e26ac9a44", null ],
    [ "name", "structruss__svcnode.html#a5ac083a645d964373f022d03df4849c8", null ],
    [ "next", "structruss__svcnode.html#a0b50d696195efdc5bce32ed7b21c2244", null ],
    [ "virtual", "structruss__svcnode.html#afabd3a4f521e3b74b53e6a9f6898a829", null ],
    [ "wildcard", "structruss__svcnode.html#ac3ef2d41f115c8a0a25319fe7a1e7a7d", null ]
];