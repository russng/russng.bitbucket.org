var annotated_dup =
[
    [ "helper_data", "structhelper__data.html", "structhelper__data" ],
    [ "russ_buf", "structruss__buf.html", "structruss__buf" ],
    [ "russ_cconn", "structruss__cconn.html", "structruss__cconn" ],
    [ "russ_conf", "structruss__conf.html", "structruss__conf" ],
    [ "russ_confitem", "structruss__confitem.html", "structruss__confitem" ],
    [ "russ_confsection", "structruss__confsection.html", "structruss__confsection" ],
    [ "russ_creds", "structruss__creds.html", "structruss__creds" ],
    [ "russ_optable", "structruss__optable.html", "structruss__optable" ],
    [ "russ_relay", "structruss__relay.html", "structruss__relay" ],
    [ "russ_relaystream", "structruss__relaystream.html", "structruss__relaystream" ],
    [ "russ_req", "structruss__req.html", "structruss__req" ],
    [ "russ_sconn", "structruss__sconn.html", "structruss__sconn" ],
    [ "russ_sess", "structruss__sess.html", "structruss__sess" ],
    [ "russ_svcnode", "structruss__svcnode.html", "structruss__svcnode" ],
    [ "russ_svr", "structruss__svr.html", "structruss__svr" ]
];