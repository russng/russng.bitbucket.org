var svr_8c =
[
    [ "HOST_NAME_MAX", "svr_8c.html#ac956117a90023ec0971b8f9fce9dec75", null ],
    [ "russ_svr_accept", "svr_8c.html#a3bdcc514db0521ee1a925c5214321164", null ],
    [ "russ_svr_free", "svr_8c.html#ae775954d000cb0bb8d3600708c981989", null ],
    [ "russ_svr_handler", "svr_8c.html#a9c5fb4055bbf0515332b46852dfda4c3", null ],
    [ "russ_svr_loop", "svr_8c.html#aa4654357d7882e8488d42cd65815b5a7", null ],
    [ "russ_svr_new", "svr_8c.html#ab4b14daa2ef34cfcd3a910aa1123bd11", null ],
    [ "russ_svr_set_accepthandler", "svr_8c.html#ae96723a5d4a131184009286f21a8b388", null ],
    [ "russ_svr_set_answerhandler", "svr_8c.html#a95db8b3bd4b84431b79e7f4064d0c9d6", null ],
    [ "russ_svr_set_autoswitchuser", "svr_8c.html#a71b41ff649778ebf59f8234e3ddc198c", null ],
    [ "russ_svr_set_closeonaccept", "svr_8c.html#ac7e2061f8d32e7cb93398dd04debd1c7", null ],
    [ "russ_svr_set_help", "svr_8c.html#a4c444b324a124496c646cb402c6f93f3", null ],
    [ "russ_svr_set_lisd", "svr_8c.html#a6661c67ec82bca5798239049283e7f03", null ],
    [ "russ_svr_set_root", "svr_8c.html#a78f40b5d273a426130bfd723113669a0", null ],
    [ "russ_svr_set_type", "svr_8c.html#a7b05b177f3553fdfa55f412ef7b6a908", null ]
];