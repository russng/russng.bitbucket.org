var structruss__relaystream =
[
    [ "bidir", "structruss__relaystream.html#a31fd92c418b4e61c77e379357dd12176", null ],
    [ "cb", "structruss__relaystream.html#a7c8ce79d6c3e7c825f735454e6b6a4ec", null ],
    [ "cbarg", "structruss__relaystream.html#aafd3d1a6343925cb274875556a6faa0e", null ],
    [ "closeonexit", "structruss__relaystream.html#a446c4bbbe349d470bae1564395faf943", null ],
    [ "nrbytes", "structruss__relaystream.html#ae7f02e36e07d709d7613235a9e6feab3", null ],
    [ "nreads", "structruss__relaystream.html#aa627eda714620692d4e42a0d6ab23a09", null ],
    [ "nwbytes", "structruss__relaystream.html#a8c44a38aa0cea8a3091ce13423b58ea4", null ],
    [ "nwrites", "structruss__relaystream.html#aeb45046d2b36879933c80b6da49e138d", null ],
    [ "rbuf", "structruss__relaystream.html#ade063bd46a44cf6be6b8073fba75b690", null ],
    [ "rfd", "structruss__relaystream.html#aedb035dbda83ff7323dd351a85347e30", null ],
    [ "rlast", "structruss__relaystream.html#ac6272a058c07fc0c06895d017e74fae6", null ],
    [ "wfd", "structruss__relaystream.html#ae14bba352616099244f58acf84818ddb", null ],
    [ "wlast", "structruss__relaystream.html#a05e31ccde358e12a68f2de9a5d4e56ea", null ]
];