var svcnode_8c =
[
    [ "russ_svcnode_add", "svcnode_8c.html#a500f13000d1e0c7ea968f624a6923691", null ],
    [ "russ_svcnode_find", "svcnode_8c.html#a4013b69679a1ed57578174e6f6de1b0d", null ],
    [ "russ_svcnode_free", "svcnode_8c.html#ac1e50deec94401e68267d2d9e9925eba", null ],
    [ "russ_svcnode_new", "svcnode_8c.html#a99bd59ad056ef013ce95a3a597c3ab32", null ],
    [ "russ_svcnode_set_autoanswer", "svcnode_8c.html#a687a591977b1a25327da49f978019269", null ],
    [ "russ_svcnode_set_handler", "svcnode_8c.html#a72ae9e146f183fc85797ccedea1e6e8a", null ],
    [ "russ_svcnode_set_virtual", "svcnode_8c.html#a7068f941bd5ef75c7f35f779ade25090", null ],
    [ "russ_svcnode_set_wildcard", "svcnode_8c.html#a37aa7a446760d01aaa65d2e03804cfe8", null ]
];