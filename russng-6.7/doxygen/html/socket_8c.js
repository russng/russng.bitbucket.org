var socket_8c =
[
    [ "_GNU_SOURCE", "socket_8c.html#a369266c24eacffb87046522897a570d5", null ],
    [ "CMSG_SIZE", "socket_8c.html#ac9a07cfab4a1060a6faa6112137a4b0c", null ],
    [ "russ_accept_deadline", "socket_8c.html#a9f80eb470c2dc2aaa080558252287fcb", null ],
    [ "russ_announce", "socket_8c.html#a981733bd6dfef6c4f751086e0ae1f735", null ],
    [ "russ_connect_deadline", "socket_8c.html#a84ef7858f6185be9f33ebcc498ce553a", null ],
    [ "russ_connectunix_deadline", "socket_8c.html#a41f73ae66199a82ee5444bd1c2ae043a", null ],
    [ "russ_get_creds", "socket_8c.html#aeb652a747306be6df83aab51cf0cb9b4", null ],
    [ "russ_recv_fd", "socket_8c.html#a7a87ddfed3a115b7ee1620c1745def22", null ],
    [ "russ_send_fd", "socket_8c.html#ab712c3b7c53082b2c24220b7f62d5bdc", null ],
    [ "russ_unlink", "socket_8c.html#aefbb5ed88a955a015847e18a3a2aa74b", null ]
];