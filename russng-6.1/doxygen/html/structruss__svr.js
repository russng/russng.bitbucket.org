var structruss__svr =
[
    [ "accepthandler", "structruss__svr.html#a30235d139f3ee715b4fd90f238c70c91", null ],
    [ "accepttimeout", "structruss__svr.html#a0de04a05829101990aa3d01811b9db28", null ],
    [ "answerhandler", "structruss__svr.html#a1c70219b917cea780c199bc311ef6fe9", null ],
    [ "autoswitchuser", "structruss__svr.html#a13834afd68cc33835bc37ecb17066297", null ],
    [ "awaittimeout", "structruss__svr.html#a1d5207b38c05ef2d3e0017350f7f3d77", null ],
    [ "closeonaccept", "structruss__svr.html#a6299977e72436665cc146389f6290267", null ],
    [ "ctime", "structruss__svr.html#a4559a6c80b6542a950e5348306e66da1", null ],
    [ "help", "structruss__svr.html#a160d8f7dc5b0c51f5e144f51aa06e576", null ],
    [ "lisd", "structruss__svr.html#a2c0c84286125871b59b9751aba726693", null ],
    [ "mpid", "structruss__svr.html#a5836aaceb496402dbcaded9e548b39fd", null ],
    [ "root", "structruss__svr.html#ac466debcccd05158c3865c720a07f11d", null ],
    [ "saddr", "structruss__svr.html#a7898fe3491e3147272e78c5e1e371aec", null ],
    [ "type", "structruss__svr.html#ac765329451135abec74c45e1897abf26", null ]
];