var searchData=
[
  ['saddr',['saddr',['../structruss__svr.html#a7898fe3491e3147272e78c5e1e371aec',1,'russ_svr']]],
  ['sconn',['sconn',['../structruss__sess.html#aafcecb2d346cfb5294d5bd95639c5047',1,'russ_sess::sconn()'],['../structhelper__data.html#a38b853f2a831c5c1a5e92288d0b9389e',1,'helper_data::sconn()']]],
  ['sd',['sd',['../structruss__cconn.html#a06b0afe769d54ae94259d8532bc878b0',1,'russ_cconn::sd()'],['../structruss__sconn.html#a06b0afe769d54ae94259d8532bc878b0',1,'russ_sconn::sd()']]],
  ['sections',['sections',['../structruss__conf.html#a6397b95f348a972f557f0017f6ace211',1,'russ_conf']]],
  ['spath',['spath',['../structruss__req.html#a48c6c60757e87b9366dff99d2fa2e6f2',1,'russ_req::spath()'],['../structruss__sess.html#af3acc0611de00e3d830b239faf8159f8',1,'russ_sess::spath()']]],
  ['str',['str',['../structruss__optable.html#a2799cca21c05a4f99f4c8b3b178d0133',1,'russ_optable']]],
  ['streams',['streams',['../structruss__relay.html#a34a8d6943a4e7c6a95669bd7f35356f9',1,'russ_relay']]],
  ['svr',['svr',['../structruss__sess.html#abb92f86255016f10462ff2bdb0edbe1b',1,'russ_sess::svr()'],['../structhelper__data.html#acbcd4d590e6fa4139fb09308871623b7',1,'helper_data::svr()']]],
  ['sysfds',['sysfds',['../structruss__cconn.html#a698a4048f272b7128b7d1d9a5442e342',1,'russ_cconn::sysfds()'],['../structruss__sconn.html#a698a4048f272b7128b7d1d9a5442e342',1,'russ_sconn::sysfds()']]]
];
