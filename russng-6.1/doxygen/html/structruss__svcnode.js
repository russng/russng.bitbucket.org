var structruss__svcnode =
[
    [ "autoanswer", "structruss__svcnode.html#aa682db27c8180700165db1398964b3c8", null ],
    [ "children", "structruss__svcnode.html#a4626683582d817cac1412aef5d605187", null ],
    [ "handler", "structruss__svcnode.html#af022afea2deca3e904e94f1e26ac9a44", null ],
    [ "name", "structruss__svcnode.html#ad547fb8186b526cb1b588daad4334fbe", null ],
    [ "next", "structruss__svcnode.html#ab1f23546655619595d2a25083033ded8", null ],
    [ "virtual", "structruss__svcnode.html#afabd3a4f521e3b74b53e6a9f6898a829", null ],
    [ "wildcard", "structruss__svcnode.html#ac3ef2d41f115c8a0a25319fe7a1e7a7d", null ]
];